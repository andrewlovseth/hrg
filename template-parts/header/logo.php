<div class="logo">
	<div class="image">
		<a href="<?php echo site_url('/'); ?>">
			<?php $image = get_field('logo', 'options'); if( $image ): ?>
				<?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
			<?php endif; ?>
		</a>
	</div>

	<div class="hamburger">
		<a href="#" class="js-nav-trigger">
			<div class="patty"></div>
		</a>
	</div>
	
	<?php get_template_part('template-parts/header/mobile-nav'); ?>
</div>
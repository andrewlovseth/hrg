<div class="map">
    <a href="<?php echo get_field('map_link'); ?>" rel="external">
        <img src="<?php $image = get_field('map_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    </a>
</div>
<?php

require_once( plugin_dir_path( __FILE__ ) . '/functions/theme-support.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/enqueue-styles-scripts.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/json-feed.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/acf.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/register-blocks.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/disable-gutenberg-editor.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/get-location.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/covid-modal.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/get-sheet-data.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/clear-sheet-data.php');

<?php

$home = get_page_by_path( 'home' );
$show = get_field('show_announcement', $home->ID);
$announcement = get_field('announcement', $home->ID);

if(get_field('announcement_background_color', $home->ID)) {
	$background_color = get_field('announcement_background_color', $home->ID);
} else {
	$background_color = $black;
}

if(get_field('announcement_text_color', $home->ID)) {
	$text_color = get_field('announcement_text_color', $home->ID);
} else {
	$text_color = $white;
}

if($show): ?>

	<style>

		<?php if($background_color): ?>
			.announcement {
				background-color: <?php echo $background_color; ?> !important;
				color: <?php echo $text_color; ?> !important;
			}
		<?php endif; ?>

		<?php if($text_color): ?>
			.announcement a,
			.announcement p {
				color: <?php echo $text_color; ?> !important;
			}
		<?php endif; ?>

	</style>

	<section class="announcement grid">
		<div class="info">
			<?php echo $announcement; ?>
		</div>
	</section>

<?php endif; ?>

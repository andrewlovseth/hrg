<?php

// COVID MODAL

function bearsmith_get_covid_modal($spreadsheet_ID, $tab_ID) {

    if($sheet !== NULL) {
        $url = "https://sheets.googleapis.com/v4/spreadsheets/" . $spreadsheet_ID . "/values/" . urlencode($tab_ID) . "!A2:Z?key=AIzaSyAJ1Hzo79gycFQGGSECizbUGo9aTW1uV5M";
    } else {
        $url = "https://sheets.googleapis.com/v4/spreadsheets/" . $spreadsheet_ID . "/values/A2:Z?key=AIzaSyAJ1Hzo79gycFQGGSECizbUGo9aTW1uV5M";
    }

    $transient_name = 'covid_info';

    if(get_transient($transient_name)) {

        $transient = get_transient($transient_name);
        return $transient;

    } else {

        $response = wp_remote_get($url);
        $api_response = json_decode( wp_remote_retrieve_body( $response ), true );
        $values = $api_response['values'];

        if($values) {
            set_transient($transient_name, $values, 600);
            return $values;
        }        
    }
}


if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_6115457304e27',
        'title' => 'Options: COVID Modal',
        'fields' => array(
            array(
                'key' => 'field_6115457bd829c',
                'label' => 'COVID',
                'name' => 'covid',
                'type' => 'group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'layout' => 'block',
                'sub_fields' => array(
                    array(
                        'key' => 'field_61154583d829d',
                        'label' => 'Show Modal',
                        'name' => 'show_modal',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '',
                        'default_value' => 0,
                        'ui' => 1,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'field_6115458fd829e',
                        'label' => 'Spreadsheet ID',
                        'name' => 'spreadsheet_id',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_6115459cd829f',
                        'label' => 'Tab ID',
                        'name' => 'tab_id',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_61158b64bb841',
                        'label' => 'Custom Content',
                        'name' => 'custom_content',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '',
                        'default_value' => 0,
                        'ui' => 1,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'field_61158b82bb842',
                        'label' => 'Headline',
                        'name' => 'headline',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_61158b64bb841',
                                    'operator' => '==',
                                    'value' => '1',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_61158b8ebb843',
                        'label' => 'Copy',
                        'name' => 'copy',
                        'type' => 'wysiwyg',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_61158b64bb841',
                                    'operator' => '==',
                                    'value' => '1',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'media_upload' => 0,
                        'delay' => 0,
                    ),
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-covid-modal',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'seamless',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
    endif;
<?php
    $args = wp_parse_args($args);
	
	if(!empty($args)) {
        $sections = $args['sections']; 
    }

    $section_title = get_sub_field('section');
    $align = get_sub_field('align');
    $columns = get_sub_field('columns');
    $template = get_sub_field('template');
    $image = get_sub_field('image');
    $notes = get_sub_field('notes');
    $specials = get_sub_field('specials');

    $className = 'menu-section';

    if($align) {
        $className .= ' align-' . $align;
    }

    if($columns) {
        $className .= ' columns-' . $columns;
    }

    if($template) {
        $className .= ' template-' . $template;
    }

    if($section_title == 'Specials' || $specials == TRUE) {
        $className .= ' specials';
    }

?>

<section class="<?php echo esc_attr($className); ?>" id="section-<?php echo sanitize_title_with_dashes($section_title); ?>">
    <?php if( $image ): ?>
        <div class="image">
            <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
        </div>                
    <?php endif; ?>

    <div class="section-header">
        <h3><?php echo $section_title; ?></h3>
    </div>
    
    <?php if($notes): ?>
        <div class="notes">
            <p><?php echo $notes; ?></p>
        </div>
    <?php endif; ?>

    <div class="items">
        <?php foreach ($sections as $section): ?>

            <?php
                
                if(isset($section[0])) {
                    $title = $section[0];
                } else {
                    $title = NULL;
                }

                if(isset($section[1])) {
                    $item = $section[1];
                } else {
                    $item = NULL;
                }

                if(isset($section[2])) {
                    $price = $section[2];
                } else {
                    $price = NULL;
                }

                if(isset($section[3])) {
                    $note = $section[3];
                } else {
                    $note = NULL;
                }

                if(isset($section[4])) {
                    $description = $section[4];
                } else {
                    $description = NULL;
                }

                if(isset($section[5])) {
                    $options = $section[5];
                } else {
                    $options = NULL;
                }

     
                if(isset($section[6]) && $section[6] == TRUE) {
                    $item_class = 'item special';
                } else {
                    $item_class = 'item';
                }                    
                

                if(isset($section[7])) {
                    $pairing = $section[7];
                } else {
                    $pairing = NULL;
                }                
            ?>

            <?php if($title == $section_title): ?>
                <div class="<?php echo $item_class; ?>">
                    <div class="title">
                        <h4><span class="name"><?php echo $item; ?></span><?php if($price): ?> <span class="price"><?php echo $price; ?></span><?php endif; ?></h4>
                    </div>

                    <div class="details">
                        <p>
                            <?php if(isset($note)): ?>
                                <span class="note"><?php echo $note; ?></span>
                            <?php endif; ?>

                            <?php if(isset($description)): ?>
                                <span class="description"><?php echo $description; ?></span>
                            <?php endif; ?>
                            
                            <?php if(isset($options)): ?>
                                <span class="options"><?php echo $options; ?></span>
                            <?php endif; ?>

                            <?php if(isset($pairing)): ?>
                                <span class="pairing"><em>tastes even better with:</em><br/><?php echo $pairing; ?></span>
                            <?php endif; ?>
                        </p>  
                    </div>              
                </div>
            <?php endif; ?>

        <?php endforeach; ?>       
    </div>
</section>
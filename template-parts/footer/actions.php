<div class="actions">
	<div class="action newsletter">
		<div class="headline">
			<h4>Sign up for our newsletter to get updates on news and events</h4>
		</div>		

		<div class="cta">
			<a href="#" class="btn" id="subscribe">Stay Connected</a>
		</div>
	</div>

	<div class="action comment">
		<div class="headline">
			<h4>Have comments or suggestions?</h4>
		</div>
		
		<div class="cta">
			<a href="mailto:<?php echo get_field('email', 'options'); ?>" class="btn">Drop Us a Line</a>
		</div>
	</div>
</div>


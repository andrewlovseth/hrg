<?php

    $header =  get_field('header');
    $gallery =  $header['gallery'];
    $left_photo = $gallery['left_photo'];
    $right_photo = $gallery['right_photo'];

    if($left_photo || $right_photo):

?>

<section class="gallery">
    <?php if( $left_photo ): ?>
        <div class="photo left-photo">
            <?php echo wp_get_attachment_image($left_photo['ID'], 'full'); ?>
        </div>
    <?php endif; ?>

    <?php if( $right_photo ): ?>
        <div class="photo right-photo">
            <?php echo wp_get_attachment_image($right_photo['ID'], 'full'); ?>
        </div>
    <?php endif; ?>
</section>

<?php endif; ?>
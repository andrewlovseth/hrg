<?php

/*
	Enqueue Styles & Scripts
*/


// Enqueue custom styles and scripts
function bearsmith_enqueue_styles_and_scripts() {
    // Register and noConflict jQuery 3.6.0
    wp_register_script( 'jquery.3.6.0', 'https://code.jquery.com/jquery-3.6.0.min.js' );
    wp_add_inline_script( 'jquery.3.6.0', 'var jQuery = $.noConflict(true);' );

    // Add style.css and third-party css
    wp_enqueue_style( 'hrg-styles', get_template_directory_uri() . '/hrg.css', '', '' );

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'hrg-plugins', get_template_directory_uri() . '/js/plugins.js', array( 'jquery.3.6.0' ), '', true );
    wp_enqueue_script( 'hrg-scripts', get_template_directory_uri() . '/js/hrg.js', array( 'jquery.3.6.0' ), '', true );
}
add_action( 'wp_enqueue_scripts', 'bearsmith_enqueue_styles_and_scripts' );
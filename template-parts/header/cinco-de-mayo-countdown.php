<?php

    $cinco = get_field('cinco_de_mayo', 'options');
    if($cinco):

?>

    <?php
        $show = $cinco['show'];
        $end_time = $cinco['end_time'];
        $message = $cinco['message'];
        if($show):    
    ?>

        <section id="cinco-de-mayo">
            <div class="wrapper">

                <div id="countdown" data-end-time="<?php echo $end_time; ?>" data-post-message="<?php echo $message; ?>">
                    <h2><?php echo $message; ?> <span id="clock"></span></h2>
                </div>

            </div>
        </section>

    <?php endif; ?>

<?php endif; ?>
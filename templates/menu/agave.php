<?php
    $args = wp_parse_args($args);
	
	if(!empty($args)) {
        $sections = $args['sections']; 
        $count = $args['count']; 
    }


    $section_title = get_sub_field('section');
    $notes = get_sub_field('notes');
    $align = get_sub_field('align');
    $columns = get_sub_field('columns');
    $template = get_sub_field('template');

    $className = 'menu-section';

    if($align) {
        $className .= ' align-' . $align;
    }

    if($columns) {
        $className .= ' columns-' . $columns;
    }

    if($template) {
        $className .= ' template-' . $template;
    }

    $intros = get_field('intro');
?>

<section class="<?php echo esc_attr($className); ?>" id="section-<?php echo sanitize_title_with_dashes($section_title); ?>">
    <div class="section-header">
        <h3><?php echo $section_title; ?></h3>
    </div>
    
    <?php if($notes): ?>
        <div class="notes">
            <p><?php echo $notes; ?></p>
        </div>
    <?php endif ?>

    <?php if($intros): ?>


        <?php
            $intro = $intros[$count];
            $label = $intro['label'];
            $headline = $intro['headline'];
            $contents = $intro['content'];
            if($contents):
        ?>

            <div class="intro agave-intro <?php echo sanitize_title_with_dashes($label); ?>-intro">
                <div class="intro-header">
                    <h4><?php echo $headline; ?></h4>
                </div>

                <?php if($contents): ?>
                    <div class="intro-contents">
                        <?php 
                            $index = 1;
                            foreach ($contents as $content):
                                $headline = $content['headline'];
                                $copy = $content['copy'];
                            ?>

                            <div class="content content-<?php echo $index; ?>">
                                <div class="headline">
                                    <h5><?php echo $headline; ?></h5>
                                </div>

                                <div class="copy">
                                    <p><?php echo $copy; ?></p>
                                </div>
                            </div>
                        <?php $index++; endforeach; ?>                
                    </div>
                <?php endif; ?>
            </div>

        <?php endif; ?>

    <?php endif; ?>

    <div class="items">
        <?php foreach ($sections as $section): ?>

            <?php
                if(isset($section[0])) {
                    $title = $section[0];
                }

                if(isset($section[1])) {
                    $item = $section[1];
                }

                if(isset($section[2])) {
                    $options = $section[2];
                }

                if(isset($section[3])) {
                    $highlight = $section[3];
                }
            ?>
            
            <?php if($title == $section_title): ?>
                <div class="item<?php if(isset($highlight)) { echo ' special'; } ?>">
                    <div class="title">
                        <h4><span class="name"><?php echo $item ?></span></h4>
                    </div>

                    <div class="details">
                        <p>
                            <?php if(isset($options)): ?>
                                <span class="options"><?php echo $options; ?></span>
                            <?php endif; ?>
                        </p>  
                    </div>              
                </div>
            <?php endif; ?>

        <?php endforeach; ?>       
    </div>
</section>
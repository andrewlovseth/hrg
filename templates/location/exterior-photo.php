<div class="exterior-photo">
    <div class="content">
        <img src="<?php $image = get_field('exterior_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    </div>
</div>
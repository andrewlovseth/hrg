<?php

/*

	Template Name: Menu Index

*/

$redirect_options = get_field('menu_redirect_options');

$default = $redirect_options['default'];
$default_url = get_permalink($default->ID);

$home_url = get_home_url();


// Build special cases redirect array
$special_cases = $redirect_options['special_cases'];
if($special_cases) {

    $redirect_array = array();
    $count = 0;
    foreach($special_cases as $special_case) {
        $start_time = $special_case['start_time'];
        $end_time = $special_case['end_time'];
        $days = $special_case['days'];
        $link = $special_case['link'];
        $link_url = get_permalink($link->ID);

        // If has day option
        if($days) {
            foreach($days as $day) {
                $redirect_array[$count]['day'] = $day;
                $redirect_array[$count]['start_time'] = $start_time;
                $redirect_array[$count]['end_time'] = $end_time;
                $redirect_array[$count]['link_url'] = $link_url;
   
                $count++;
            }

        // If doesn't have day option
        } else {
            $redirect_array[$count]['start_time'] = $start_time;
            $redirect_array[$count]['end_time'] = $end_time;
            $redirect_array[$count]['link_url'] = $link_url;
    
            $count++;
        }        
    }
}


if($default_url !== NULL) {
    $use_special_case = TRUE;
    if($redirect_array !== NULL) {

        foreach($redirect_array as $redirect_option) {
            $day = $redirect_option['day'];
            $start_time = $redirect_option['start_time'];
            $start = date("H", strtotime($start_time));
            $end_time = $redirect_option['end_time'];
            $end = date("H", strtotime($end_time));
            $link_url = $redirect_option['link_url'];
            //var_dump($start_time);

            if($start_time !== '') {
                if($day) {
                    if(wp_date('l') === $day && wp_date('H') >= $start && wp_date('H') < $end) {
                        header("Location: " . $link_url);
                    } else {
                        $use_special_case = false;
                    }
                    //var_dump($use_special_case);
                } else {
                    if(wp_date('H') >= $start && wp_date('H') < $end) {
                        header("Location: " . $link_url);
                    } else {
                        $use_special_case = false;
                    }

                    //var_dump($use_special_case);
                }
            } else {
                header("Location: " . $default_url);
            }
        }

    } else {
        header("Location: " . $default_url);
    }
    
    if($use_special_case == false || $redirect_array == NULL) {
        header("Location: " . $default_url);
    }
} 
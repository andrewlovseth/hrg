<?php
// Create class attribute allowing for custom "className" and "align" values.
$className = 'nameplate';

if(get_field('show_status_view', 'options')) {
    $className .= ' status-view';
}
?>

<div class="<?php echo esc_attr($className); ?>">
    <?php get_template_part('template-parts/header/popup'); ?>

    <div class="nameplate-wrapper">
        <?php get_template_part('template-parts/header/default-view'); ?>
    </div>
</div>
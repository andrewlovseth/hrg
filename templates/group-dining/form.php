<?php

    $form = get_field('form');
    $headline = $form['headline'];
    $shortcode = $form['shortcode'];
    $post_form_copy = $form['post_form_copy'];
    $footer_copy = $form['footer_copy'];

?>

<section class="form">

    <div class="section-header">
        <h3><?php echo $headline; ?></h3>
    </div>

    <?php echo do_shortcode($shortcode); ?>

    <div class="post-form-copy">
        <?php echo $post_form_copy; ?>
    </div>

    <div class="footer-copy">
        <?php echo $footer_copy; ?>
    </div>

</section>
<?php

function bearsmith_get_json_cache($spreadsheetID, $tab) {
    $url = "https://spreadsheets.google.com/feeds/list/" . $spreadsheetID . "/" . $tab . "/public/values?alt=json";
    $stream_opts = [
        "ssl" => [
            "verify_peer" => false,
            "verify_peer_name" => false,
        ]
    ];    

    $json = file_get_contents($url, false, stream_context_create($stream_opts));
    $feed = json_decode($json, true);
    $entries = $feed['feed']['entry'];

    return $entries;
}
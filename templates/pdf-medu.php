<?php

/*

	Template Name: PDF Menu

*/

    $file = get_field('file');
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: " . $file['url']);
    exit();

?>
	</main> <!-- .site-content -->

	<footer class="site-footer grid">
		<?php get_template_part('template-parts/footer/actions'); ?>
			
		<?php get_template_part('template-parts/footer/hrg-family'); ?>

		<?php get_template_part('template-parts/footer/photo-credit'); ?>
	</footer>

	<?php get_template_part('template-parts/footer/newsletter-modal'); ?>

	<?php get_template_part('template-parts/global/covid-modal'); ?>

<?php wp_footer(); ?>

</div> <!-- .site -->

</body>
</html>
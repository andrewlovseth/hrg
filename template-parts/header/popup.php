<?php if(have_rows('popup', 'options')): while(have_rows('popup', 'options')) : the_row(); ?>

    <?php if( get_row_layout() == 'link' ): ?>

		<div class="popup">
			<?php if(get_sub_field('url')): ?>
				<a href="<?php echo get_sub_field('url'); ?>" class="popup-link" rel="external"><span><?php echo get_field('popup_link_text', 'options'); ?></span></a>
			<?php else: ?>
				<span class="popup-link" rel="external"><span><?php echo get_field('popup_link_text', 'options'); ?></span></span>

			<?php endif; ?>
		</div>
	
    <?php endif; ?>
 
    <?php if( get_row_layout() == 'graphic' ): ?>

		<div class="popup">
			<a href="#" class="popup-trigger"><span><?php echo get_field('popup_link_text', 'options'); ?></span></a>
		</div>

		<section class="popup-overlay">
			<div class="overlay">
						
				<div class="overlay-wrapper graphic">
					
					<a href="#" class="popup-close close">✕</a>

					<div class="image">
						<a href="<?php echo get_sub_field('link'); ?>" rel="external">
							<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>
					</div>
				</div>

			</div>
		</section>

    <?php endif; ?>
 
    <?php if( get_row_layout() == 'photo_and_text' ): ?>
		
		<div class="popup">
			<a href="#" class="popup-trigger"><span><?php echo get_field('popup_link_text', 'options'); ?></span></a>
		</div>

		<section class="popup-overlay">
			<div class="overlay">

				<?php 
					$link = get_sub_field('cta');
					if( $link ) {
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					}
				?>

				<div class="overlay-wrapper photo-and-text">
					
					<a href="#" class="popup-close close">✕</a>

					<?php if(get_sub_field('photo')): ?>
						<div class="image">
							<a href="<?php echo esc_url( $link_url ); ?>" rel="external">
								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>
					<?php endif; ?>

					<div class="info">
						<?php if(get_sub_field('headline')): ?>
							<div class="headline">
								<h3><?php echo get_sub_field('headline'); ?></h3>
							</div>
						<?php endif; ?>

						<?php if(get_sub_field('sub_headline')): ?>
							<div class="sub-headline">
								<h4><?php echo get_sub_field('sub_headline'); ?></h4>
							</div>
						<?php endif; ?>

						<?php if(get_sub_field('copy')): ?>
							<div class="copy">
								<?php echo get_sub_field('copy'); ?>
							</div>
						<?php endif; ?>

						<?php if( $link ): ?>
							<div class="btn">
						    	<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						    </div>						    
						<?php endif; ?>

					</div>
				</div>

			</div>
		</section>
						
    <?php endif; ?>

<?php endwhile; endif; ?>
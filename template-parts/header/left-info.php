<div class="info hours left">
	<?php if(get_field('hours_header', 'option')): ?>
		<h3><?php echo get_field('hours_header', 'option'); ?></h3>
	<?php else: ?>
		<h3>Hours</h3>
	<?php endif; ?>

	<?php echo get_field('hours', 'options'); ?>
</div>
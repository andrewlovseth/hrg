<div class="info">
    <h3><?php echo get_field('hours_header', 'options'); ?></h3>
    <?php echo get_field('hours', 'options'); ?>
    <?php the_content(); ?>
</div>
<?php

/*
    Advanced Custom Fields
*/


// Add options pages
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

if( function_exists('acf_add_options_sub_page') ) {
    acf_add_options_sub_page('Global');
	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Footer');
    acf_add_options_sub_page('Popup');
    acf_add_options_sub_page('COVID Modal');
}


// Order Relationship fields
function bearsmith_relationship_order_by_date($args, $field, $post_id) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}
add_filter('acf/fields/relationship/query', 'bearsmith_relationship_order_by_date', 10, 3);


// Custom back-end styles
function bearsmith_acf_styles() {
    ?>

        <style type="text/css">
            .acf-relationship .list {
                height: 400px;
            }
        </style>

    <?php
}
add_action('acf/input/admin_head', 'bearsmith_acf_styles');
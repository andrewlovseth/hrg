<?php

function bearsmith_get_location($post) {
	$parent = wp_get_post_parent_id($post);
	$parent_template = get_page_template_slug($parent);

    $parent_post = get_post($post->post_parent);
    $grandparent = $parent_post->post_parent;
    $grandparent_template = get_page_template_slug($grandparent);
    $grandparent_id = wp_get_post_parent_id($parent_post);

	if ( is_page_template( 'templates/home.php' ) ) {
		$slug = $post->post_name;
		return $slug;
	} elseif($parent_template == 'templates/home.php') {		
		$slug = get_post_field( 'post_name', $parent );
		return $slug;	
	} elseif($grandparent_template == 'templates/home.php') {		
		$slug = get_post_field( 'post_name', $grandparent_id );
		return $slug;	
    } else {
		return null;
	}
}
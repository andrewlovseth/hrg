<?php

/*

	Template Name: Menu

*/

get_header(); 

$meta = get_field('meta');
$theme = $meta['theme'];
$className = 'menu grid';

if($theme) {
	$className .= ' theme-' . $theme;
}

$name = $post->post_name;    
$spreadsheet_ID = $meta['spreadsheet_id'];
$tab_ID = $meta['tab_id'];
$location = bearsmith_get_location($post); 

if($location) {
	$menu_transient_name = 'HRG_' . $post->post_name . '_' . $location . '_menu';
} else {
	$menu_transient_name = 'HRG_' . $post->post_name . '_menu';
}

if(get_transient($menu_transient_name)) {
	$entries = get_transient($menu_transient_name);
} else {
	$entries = bearsmith_get_sheet_data($menu_transient_name, $spreadsheet_ID, 300, $tab_ID);
}

?>

<section class="<?php echo esc_attr($className); ?>">

	<?php get_template_part('templates/menu-index/subnav'); ?>

		<?php $count = 0; if(have_rows('menu')): ?>
			<?php while(have_rows('menu')): the_row(); ?>

				<?php
					$section_title = get_sub_field('section');
					$template = get_sub_field('template');
					$allowed = [$section_title];
					$sections = array_filter(
						$entries,
						function ($entry) use ($allowed) {
							if(isset($entry[0])) {
								return in_array($entry[0], $allowed);
							}
						}
					);

					if(!empty($sections)) {
						$args = [
							'sections' => $sections,
							'count' => $count
						];

						get_template_part('templates/menu/' . $template, null, $args);
					}
				?>
					
			<?php $count++; endwhile; ?>
		<?php endif; ?>

		<?php get_template_part('templates/menu-index/footer-copy'); ?>

	</section>

<?php get_footer(); ?>
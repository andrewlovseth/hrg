<div class="gallery">
    <?php $photoIDs = ''; $images = get_field('gallery'); if( $images ): ?>
        <?php foreach( $images as $image ): ?>						
            <?php $photoIDs .= $image['ID'] . ','; ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php echo do_shortcode('[gallery ids="' . $photoIDs . '"]'); ?>
</div>
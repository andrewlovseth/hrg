

<?php

    $covid = get_field('covid', 'options');
    $show_modal = $covid['show_modal'];
    $spreadsheet_id = $covid['spreadsheet_id'];
    $tab_id = $covid['tab_id'];
    $custom_content = $covid['custom_content'];


    if(get_transient('covid_info')) {
        $modal_data = get_transient('covid_info');
    } else {
        $modal_data = bearsmith_get_covid_modal($spreadsheet_id, $tab_id);
    }

    if($custom_content == TRUE) {
        $headline = $covid['headline'];
        $copy = $covid['copy'];
    } else {
        $headline = $modal_data[0][0];
        $copy = $modal_data[0][1];
    }

    if( $show_modal == TRUE && (is_front_page() == TRUE ) || ( $show_modal == TRUE && is_page_template('templates/home.php')) ):

?>

    <section id="covid-modal">
        <div class="overlay">
            <div class="overlay-wrapper">
                    
                <a href="#" class="close covid-close-btn">✕</a>
            
                <div class="headline">
                    <h2><?php echo $headline; ?></h2>
                </div>
                
                <div class="content">
                    <?php echo $copy; ?>
                </div>

                <script type="text/javascript">

                    (function ($, window, document, undefined) {
                        $(document).ready(function($) {

                            var visits = 1;
                            //check if sessionStorage item exisits
                            //If it does, increment it.
                            if (sessionStorage.visits) {
                                visits = visits + 1;
                                sessionStorage.setItem("visits", visits);
                            }
                            //If it doesn't, create it!
                            else if (!sessionStorage.visits) {
                                sessionStorage.setItem("visits", visits);
                            }

                            if(sessionStorage.visits == 1) {
                                $('#covid-modal').fadeIn('slow');
                            }

                            // COVID Modal Hide
                            $('.covid-close-btn').click(function(){
                                $('#covid-modal').fadeOut(200);
                                return false;
                            });

                            $(document).keyup(function(e) {		
                                if (e.keyCode == 27) {
                                    $('#covid-modal').fadeOut(200);
                                }
                            });

                        });
                    })(jQuery, window, document);

                </script>

            </div>
        </div>
    </section>

<?php endif; ?>


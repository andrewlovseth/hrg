<?php if(have_rows('navigation','options')): ?>
	<nav class="site-nav">
		<ul>
			<?php while(have_rows('navigation', 'options')): the_row(); ?>

				<?php if(get_sub_field('dropdown')): ?>

					<?php get_template_part('template-parts/header/site-nav-dropdown'); ?>

				<?php else: ?>

					<?php get_template_part('template-parts/header/site-nav-link'); ?>

				<?php endif; ?>

			<?php endwhile; ?>
		</ul>
	</nav>
<?php endif; ?>
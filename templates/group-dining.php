<?php

/*

	Template Name: Group Dining

*/

get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="group-dining grid">
			<?php get_template_part('templates/group-dining/header'); ?>

			<?php get_template_part('templates/group-dining/gallery'); ?>

			<?php get_template_part('templates/group-dining/faqs'); ?>

			<?php get_template_part('templates/group-dining/form'); ?>
        </section>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>
<?php
    $args = wp_parse_args($args);
	
	if(!empty($args)) {
        $sections = $args['sections']; 
    }

    $section_title = get_sub_field('section');
    $align = get_sub_field('align');
    $columns = get_sub_field('columns');
    $template = get_sub_field('template');
    $image = get_sub_field('image');
    $notes = get_sub_field('notes');
    $specials = get_sub_field('specials');

    $className = 'menu-section';

    if($align) {
        $className .= ' align-' . $align;
    }

    if($columns) {
        $className .= ' columns-' . $columns;
    }

    if($template) {
        $className .= ' template-' . $template;
    }

    if($section_title == 'Specials' || $specials == TRUE) {
        $className .= ' specials';
    }

?>

<section class="<?php echo esc_attr($className); ?>" id="section-<?php echo sanitize_title_with_dashes($section_title); ?>">
    <?php if( $image ): ?>
        <div class="image">
            <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
        </div>                
    <?php endif; ?>

    <div class="section-header">
        <h3><?php echo $section_title; ?></h3>
    </div>
    
    <?php if($notes): ?>
        <div class="notes">
            <p><?php echo $notes; ?></p>
        </div>
    <?php endif; ?>

    <div class="items">
        <?php foreach ($sections as $section): ?>

            <?php
                $title = $section[0];
                $item = $section[1];
                $price = $section[2];
                $note = $section[3];
                $description = $section[4];
                $highlight = $section[5];
                $duo = $section[6];
                $wine_1_name = $section[7];
                $wine_1_vintage = $section[8];
                $wine_1_description = $section[9];
                $wine_1_price = $section[10];
                $wine_2_name = $section[11];
                $wine_2_vintage = $section[12];
                $wine_2_description = $section[13];
                $wine_2_price = $section[14];
            ?>


            <?php if($title == $section_title): ?>

                <?php if($duo == 'TRUE'): ?>
                    <div class="item duo<?php if($higlight == 'TRUE') { echo ' special'; } ?>">
                        <div class="title">
                            <h4><span class="name"><?php echo $item; ?></span><?php if($price): ?> <span class="price"><?php echo $price; ?></span><?php endif; ?></h4>
                        </div>

                        <div class="duo-details">
                            <div class="wine wine-1">
                                <div class="vintage">
                                    <p><?php echo $wine_1_vintage; ?></p>
                                </div>

                                <div class="info">
                                    <p>
                                        <span class="name"><?php echo $wine_1_name; ?></span> · <span class="price"><?php echo $wine_1_price; ?></span> <br/>
                                        <?php if($wine_1_description): ?><span class="description"><?php echo $wine_1_description; ?></span><?php endif; ?>                                  
                                    </p>                                
                                </div>                            
                            </div>

                            <div class="wine wine-2">
                                <div class="vintage">
                                    <p><?php echo $wine_2_vintage; ?></p>
                                </div>

                                <div class="info">
                                    <p>
                                        <span class="name"><?php echo $wine_2_name; ?></span> · <span class="price"><?php echo $wine_2_price; ?></span> <br/>
                                        <?php if($wine_2_description): ?><span class="description"><?php echo $wine_2_description; ?></span><?php endif; ?>                                  
                                    </p>                                
                                </div>                            
                            </div>
                        </div>              
                    </div>

                <?php else: ?>

                    <div class="item<?php if($highlight == 'TRUE') { echo ' special'; } ?>">
                        <div class="title">
                            <h4><span class="name"><?php echo $item; ?></span><?php if($price): ?> <span class="price"><?php echo $price; ?></span><?php endif; ?></h4>
                        </div>

                        <div class="details">
                            <p>
                                <?php if($note): ?>
                                    <span class="note"><?php echo $note; ?></span>
                                <?php endif; ?>

                                <?php if($description): ?>
                                    <span class="description"><?php echo $description; ?></span>
                                <?php endif; ?>
                            </p>  
                        </div>              
                    </div>
                <?php endif; ?>
            <?php endif; ?>

        <?php endforeach; ?>       
    </div>
</section>
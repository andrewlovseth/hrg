<?php

    $parent_ID = wp_get_post_parent_id($post->ID);
    $page_ID = $post->ID;

?>

<nav class="menu-subnav">
    <ul>
        <?php if(have_rows('menu_subnav', $parent_ID)): while(have_rows('menu_subnav', $parent_ID)): the_row(); ?>
        
            
        <?php 
            $link = get_sub_field('link');
            if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_page = url_to_postid( $link_url );
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
            <?php if(get_sub_field('show') == TRUE): ?>
                <li>
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" <?php if($link_page === $page_ID ): ?> class="active"<?php endif; ?>>
                        <?php echo esc_html($link_title); ?>
                    </a>
                </li>
            <?php endif; ?>
        <?php endif; ?>


        <?php endwhile; endif; ?>
    </ul>
</nav>
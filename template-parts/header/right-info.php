<div class="info location right">
	<?php if(get_field('location_header', 'option')): ?>
		<h3><?php echo get_field('location_header', 'option'); ?></h3>
	<?php else: ?>
		<h3>Location</h3>
	<?php endif; ?>
	
	<?php echo get_field('location', 'options'); ?>
</div>
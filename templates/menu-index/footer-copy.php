<?php

    $menu_index = $post->post_parent;
    $copy = get_field('menu_footer_copy', $menu_index);
    if($copy):

?>

    <div class="footer-copy">
        <?php echo $copy; ?>
    </div>

<?php endif; ?>
<?php

    $header =  get_field('header');
    $menu_pdf = $header['menu_pdf'];
?>

<section class="faqs">
    <?php if(get_field('faqa_headline')): ?>
        <div class="section-header">
            <h3><?php echo get_field('faqs_headline'); ?></h3>
        </div>
    <?php endif; ?>

    <?php if(have_rows('faqs')): ?>
        <div class="faqs-list">
            <?php while(have_rows('faqs')) : the_row(); ?>

                <?php if( get_row_layout() == 'faq' ): ?>

                    <div class="faq">
                        <div class="question">
                            <h4><?php echo get_sub_field('question'); ?></h4>
                        </div>

                        <div class="answer">
                            <?php echo get_sub_field('answer'); ?>
                        </div>
                    </div>
                <?php endif; ?>
            
            <?php endwhile; ?>
        </div>
    <?php endif; ?>

    <?php if($menu_pdf['url']): ?>
        <div class="cta">
            <a href="<?php echo $menu_pdf['url']; ?>" class="btn">View Menus</a>
        </div>
    <?php endif; ?>

</section>
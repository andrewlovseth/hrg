<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php echo get_field('head_meta', 'options'); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php get_template_part('template-parts/header/styles'); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<?php echo get_field('body_meta', 'options'); ?>

<div id="page" class="site">
	<?php get_template_part('template-parts/header/utility-nav'); ?>
	<?php get_template_part('template-parts/header/announcement'); ?>
	<?php get_template_part('template-parts/header/cinco-de-mayo-countdown'); ?>

	<header class="site-header grid">
		<?php get_template_part('template-parts/header/site-nav'); ?>
		<?php get_template_part('template-parts/header/social'); ?>
		<?php get_template_part('template-parts/header/main-header'); ?>
	</header>
	

	<main class="site-content">
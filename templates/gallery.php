<?php

/*

	Template Name: Gallery

*/

get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="gallery grid">
			<?php get_template_part('template-parts/global/page-header'); ?>

			<?php get_template_part('templates/gallery/grid'); ?>
		</section>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>
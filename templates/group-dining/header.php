<?php

    $header =  get_field('header');
    $headline = $header['headline'];
    $copy = $header['copy'];

?>

<section class="header">
    <div class="section-header">
        <h3><?php echo $headline; ?></h3>
    </div>

    <div class="copy">
        <?php echo $copy; ?>
    </div>
</section>
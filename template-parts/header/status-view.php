<div class="status">

	<?php get_template_part('template-parts/header/logo'); ?>

	<div class="message">
		<div class="headline">
			<h2><?php echo get_field('status_headline', 'options'); ?></h2>
		</div>

		<div class="copy">
			<?php echo get_field('status_copy', 'options'); ?>
		</div>
	</div>

</div>
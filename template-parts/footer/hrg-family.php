<section class="hrg-family">

	<div class="footer-header">
		<h4><a href="http://heavyrg.com/" rel="external">The Heavy Restaurant Group Family</a></h4>
	</div>

	<div class="restaurants">
		<div class="row">
			<div class="link purple">
				<a href="http://purplecafe.com/" rel="external">Purple</a>
			</div>

			<div class="link barrio">
				<a href="http://barriorestaurant.com/" rel="external">Barrio</a>
			</div>

			<div class="link fiasco">
				<a href="https://fiascoseattle.com/" rel="external">Fiasco</a>
			</div>

			<div class="link pablo-y-pablo">
				<a href="http://pabloypablo.com/" rel="external">Pablo y Pablo</a>
			</div>
		</div>

		<div class="row">


			<div class="link livbud">
				<a href="https://www.livbudcafe.com/" rel="external">Livbud</a>
			</div>

			<div class="link detour-bar">
				<a href="https://www.detourbarseattle.com/" rel="external">Detour Bar</a>
			</div>
	
			<div class="link heavy-catering">
				<a href="https://heavyrg.com/catering/" rel="external">Heavy Catering</a>
			</div>
		</div>
	</div>

</section>
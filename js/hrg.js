(function ($, window, document, undefined) {
    $(document).ready(function () {
        // rel="external"
        $('a[rel="external"]').click(function () {
            window.open($(this).attr("href"));
            return false;
        });

        // Menu Toggle
        $(".js-nav-trigger").click(function () {
            $("body").toggleClass("nav-open");
            $(".mobile-nav").slideToggle(300);
            return false;
        });

        // Open Newsletter Subscription
        $("#subscribe").on("click", function () {
            $("#newsletter-subscribe").fadeIn(25, function () {
                $("#newsletter-subscribe .overlay").addClass("show");
            });

            return false;
        });

        // Close Newsletter Subscription
        $("#newsletter-subscribe a.close").on("click", function () {
            $("#newsletter-subscribe.overlay").removeClass("show");
            setTimeout(function () {
                $("#newsletter-subscribe").hide();
            }, 500);
            return false;
        });

        // Open Popup
        $("a.popup-trigger").on("click", function () {
            $(".popup-overlay").fadeIn(25, function () {
                $(".popup-overlay .overlay").addClass("show");
            });

            $("body").addClass("popup-overlay-open");

            return false;
        });

        // Close Popup
        $(".popup-close").on("click", function () {
            $("body").removeClass("popup-overlay-open");

            $(".popup-overlay .overlay").removeClass("show");
            setTimeout(function () {
                $(".popup-overlay").hide();
            }, 500);

            return false;
        });

        // Close Heavy at Home
        $("#heavy-at-home a.close").on("click", function () {
            $("#heavy-at-home .overlay").removeClass("show");
            setTimeout(function () {
                $("#heavy-at-home").hide();
            }, 500);
            return false;
        });

        // Cinco de May countdown clock
        var endTime = $("#countdown").data("end-time");
        var postMessage = $("#countdown").data("post-message");

        $("#clock")
            .countdown(endTime)
            .on("update.countdown", function (event) {
                var format = "%-D days %-H hours %-M minutes %-S seconds";

                $(this).html(event.strftime(format));
            })
            .on("finish.countdown", function (event) {
                $("#countdown h2").html(postMessage);
            });
    });

    // Heavy at Home Modal
    if (sessionStorage.pageCount) {
        sessionStorage.pageCount = Number(sessionStorage.pageCount) + 1;
    } else {
        sessionStorage.pageCount = 1;
    }

    if (sessionStorage.pageCount == 1) {
        $("#heavy-at-home").addClass("show");
    }

    $(".site-nav__target-link").on("click", function (event) {
        $(this).parent(".site-nav__target").toggleClass("open");
        return false; // Prevents the default action
    });

    // To specifically stop the propagation when nested links are clicked:
    $(".site-nav__target a").on("click", function (event) {
        event.stopPropagation(); // Prevents the click event from bubbling up to the `.site-nav__target`
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $(".popup-overlay .overlay, #heavy-at-home .overlay").removeClass("show");
            setTimeout(function () {
                $(".popup-overlay, #heavy-at-home").hide();
            }, 500);
        }
    });
})(jQuery, window, document);
